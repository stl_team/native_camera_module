import React, {useState} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import SplashScreen from '../component/screens/Splash';
import HomeScreen from '../component/screens/Home';
import HigherOrderComponent from '../hoc/HigherOrderComponent';

const Stack = createStackNavigator();

const bindHOC = (component) => {
	return HigherOrderComponent(component);
};

export default function AppContainer() {
  const [Visible, setVisible] = useState(true);


  setTimeout(function () {
    setVisible(false);
  }, 1500);

  return (
		<NavigationContainer>
			<Stack.Navigator>
				{Visible ? (
					<>
						<Stack.Screen
							name="Splash"
							component={bindHOC(SplashScreen)}
							options={{ headerShown: false }}
						/>
					</>
				) : (
					<>
						<Stack.Screen name="Capture Image" component={bindHOC(HomeScreen)} />
					</>
				)}
			</Stack.Navigator>
		</NavigationContainer>
  );
}
