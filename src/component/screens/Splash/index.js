import React from 'react';
import {View, Text} from 'react-native';
import styles from './style';
export default class SplashScreen extends React.Component {
  render() {
    return (
			<View style={styles.main}>
				<View style={styles.body}>
					<Text style={{ fontSize: 35}}>Welcome</Text>
				</View>
			</View>
	);
  }
}
