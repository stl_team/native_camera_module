import { StyleSheet, Dimensions } from 'react-native';
const windowWidth = Dimensions.get('window').width;

export default StyleSheet.create({
	main: {
		flex: 1,
		alignItems: 'center',
	},
	imageView: {
		height: '80%',
		width: '90%',
		marginTop: '5%',
	},
	image: {
		height: '100%',
		width: '100%',
		borderRadius: 15,
		resizeMode: 'cover',
	},
	buttonView: {
		alignItems: 'center',
		width: '100%',
	},
	button: {
		height: 40,
		width: windowWidth - 10,
		backgroundColor: '#1c3aff',
		alignItems: 'center',
		justifyContent: 'center',
		borderRadius: 5,
		marginTop: 15,
	},
	txt: {
		color: '#fff',
		fontSize: 17,
		fontWeight: 'bold',
	},
});
