import React from "react";
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  NativeModules,
} from "react-native";
import styles from "./style";

export const MyCamera = NativeModules.MyCamera;
export const CameraBridge = NativeModules.CameraBridge;

class HomeScreen extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      imageUri: null,
    };
  }

  onCameraCaptureClick = () => {
    if (Platform.OS === "android") {
      MyCamera.openMyCameraClick().then((result) => {
        if (result) {
          this.setState({ imageUri: result });
        }
      });
    } else {
      CameraBridge.ChangeViewBridge().then((result) => {
        console.log("GET_IMAGEPATHHERE==========>" + result);
        if (result) {
          this.setState({ imageUri: result });
        }
      });
    }
  };

  render() {
    return (
      <View style={styles.main}>
        <View style={styles.imageView}>
          <TouchableOpacity onPress={() => this.onCameraCaptureClick()}>
            <Image
              style={styles.image}
              source={
                this.state.imageUri
                  ? {
                      uri: this.state.imageUri,
                    }
                  : require("../../../images/Camera.jpg")
              }
            />
          </TouchableOpacity>
        </View>

        <View style={styles.buttonView}>
          <TouchableOpacity onPress={() => this.onCameraCaptureClick()}>
            <View style={styles.button}>
              <Text style={styles.txt}>Capture Image</Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
export default HomeScreen;
