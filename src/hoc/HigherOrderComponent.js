import React from 'react';
import {ImageBackground, Platform} from 'react-native';
import backgroundImage from '../images/backgroundImage.png';
import styles from './Style';
export default (WrappedComponent) => {
  class ConnectedComponent extends React.Component {
    render() {
      const isAndroid = Platform.OS === 'android';
      return (
        <ImageBackground
          source={backgroundImage}
          style={styles.backgroundImage}>
          <WrappedComponent {...this.props} isAndroid={isAndroid} />
        </ImageBackground>
      );
    }
  }
  return ConnectedComponent;
};
