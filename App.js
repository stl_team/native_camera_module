import React from 'react';
import AppContainer from './src/router/router';

const App = () => {
	return <AppContainer />;
};

export default App;
