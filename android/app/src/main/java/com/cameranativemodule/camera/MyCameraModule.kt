package com.cameranativemodule.camera

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Environment
import android.util.Log
import androidx.core.content.FileProvider
import com.facebook.react.bridge.*
import com.theartofdev.edmodo.cropper.CropImage
import java.io.File

class MyCameraModule internal constructor(private var reactContext: ReactApplicationContext) : ReactContextBaseJavaModule(reactContext) {
    private var myCamera: MyCamera? = null;

    private var mPickerPromise: Promise? = null

    /**
     * Handle camera capture result
     */
    private val mActivityEventListener: ActivityEventListener = object : BaseActivityEventListener() {
        override fun onActivityResult(activity: Activity, requestCode: Int, resultCode: Int, intent: Intent?) {
            if (requestCode == REQUEST_IMAGE_CAPTURE) {
                //Handle capture image
                if (mPickerPromise != null) {
                    if (resultCode == Activity.RESULT_CANCELED) {
                        mPickerPromise!!.reject(E_PICKER_CANCELLED, "Image picker was cancelled")
                    } else if (resultCode == Activity.RESULT_OK) {

                        val currentPhotoPath = myCamera?.currentPhotoPath

                        val file = File(Environment.getExternalStorageDirectory(), currentPhotoPath)

                        val capturedPhotoUri = currentActivity?.let { FileProvider.getUriForFile(it, currentActivity!!.getPackageName() + ".provider", file) }

                        currentActivity?.let {
                            CropImage.activity(Uri.fromFile(File(currentPhotoPath)))
                                    .start(it)
                        }
                    }
                }
            } else if (requestCode == IMAGE_PICKER_REQUEST) {
                //Handle pick image
                if (mPickerPromise != null) {
                    if (resultCode == Activity.RESULT_CANCELED) {
                        mPickerPromise!!.reject(E_PICKER_CANCELLED, "Image picker was cancelled")
                    } else if (resultCode == Activity.RESULT_OK) {
                        val uri: Uri? = intent?.data

                        if (uri == null) {
                            mPickerPromise!!.reject(E_NO_IMAGE_DATA_FOUND, "No image data found")
                        } else {
                            currentActivity?.let {
                                CropImage.activity(uri)
                                        .start(it)
                            }
                        }

                    }
                }
            } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                //Handle crop image
                if (mPickerPromise != null) {
                    if (resultCode == Activity.RESULT_OK) {
                        val result = CropImage.getActivityResult(intent)
                        val uri: Uri = result.getUri()

                        mPickerPromise!!.resolve(uri.toString())
                    } else {
                        mPickerPromise!!.reject(E_PICKER_CANCELLED, "Image picker was cancelled")
                        mPickerPromise = null
                    }

                    mPickerPromise = null
                }
            }
        }
    }


    override fun getName(): String {
        return "MyCamera"
    }

    /**
     * Call method from React Native to capture camera
     */
    @ReactMethod
    fun openMyCameraClick(promise: Promise) {
        try {
            val currentActivity = currentActivity
            if (currentActivity == null) {
                promise.reject(E_ACTIVITY_DOES_NOT_EXIST, "Activity doesn't exist")
                return
            }

            mPickerPromise = promise
            try {
                currentActivity.let { myCamera?.openMyCamera(it, REQUEST_IMAGE_CAPTURE) }
            } catch (e: Exception) {
                mPickerPromise!!.reject(E_FAILED_TO_SHOW_PICKER, e)
                mPickerPromise = null
            }
        } catch (e: Exception) {
            promise.reject(e)
        }
    }

    /**
     * Call method from React Native to fetch image from gallery
     */
    @ReactMethod
    fun fetchGalleryClick(promise: Promise) {
        try {
            val currentActivity = currentActivity
            if (currentActivity == null) {
                promise.reject(E_ACTIVITY_DOES_NOT_EXIST, "Activity doesn't exist")
                return
            }

            // Store the promise to resolve/reject when picker returns data
            mPickerPromise = promise
            try {
                currentActivity.let { myCamera?.fetchGallery(it, IMAGE_PICKER_REQUEST) }
            } catch (e: Exception) {
                mPickerPromise!!.reject(E_FAILED_TO_SHOW_PICKER, e)
                mPickerPromise = null
            }
        } catch (e: Exception) {
            promise.reject(e)
        }
    }

    companion object {
        private const val TAG = "MyCameraModule"
        private const val REQUEST_IMAGE_CAPTURE = 1
        private const val IMAGE_PICKER_REQUEST = 2
        private const val E_ACTIVITY_DOES_NOT_EXIST = "E_ACTIVITY_DOES_NOT_EXIST"
        private const val E_PICKER_CANCELLED = "E_PICKER_CANCELLED"
        private const val E_FAILED_TO_SHOW_PICKER = "E_FAILED_TO_SHOW_PICKER"
        private const val E_NO_IMAGE_DATA_FOUND = "E_NO_IMAGE_DATA_FOUND"
    }

    init {
        myCamera = MyCamera.instance
        reactContext.addActivityEventListener(mActivityEventListener)
    }
}