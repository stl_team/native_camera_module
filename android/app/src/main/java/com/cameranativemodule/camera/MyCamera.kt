package com.cameranativemodule.camera

import android.annotation.SuppressLint
import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Intent
import android.os.Environment
import android.provider.MediaStore
import androidx.core.content.FileProvider
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

class MyCamera {

    var currentPhotoPath = "";

    /**
     * Open camera using intent
     */
    fun openMyCamera(mCurrentActivity: Activity, requestCode: Int) {
        try {
            val m_intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            val imageFile: File = createImageFile(mCurrentActivity)
            val uri = FileProvider.getUriForFile(mCurrentActivity, mCurrentActivity.getPackageName() + ".provider", imageFile)
            m_intent.putExtra(MediaStore.EXTRA_OUTPUT, uri)
            mCurrentActivity.startActivityForResult(m_intent, requestCode)

        } catch (e: ActivityNotFoundException) {
           e.printStackTrace()
        }
    }

    /**
     * Open gallery using intent
     */
    fun fetchGallery(mCurrentActivity: Activity, requestCode: Int) {
        val currentActivity = mCurrentActivity
        try {
            val galleryIntent = Intent(Intent.ACTION_PICK)
            galleryIntent.type = "image/*"
            val chooserIntent = Intent.createChooser(galleryIntent, "Pick an image")
            currentActivity.startActivityForResult(chooserIntent, requestCode)
        } catch (e: ActivityNotFoundException) {
            e.printStackTrace()
        }
    }

    /**
     * Create temp file for capture image
     */
    @SuppressLint("SimpleDateFormat")
    @Throws(IOException::class)
    private fun createImageFile(mCurrentActivity: Activity): File {
        // Create an image file name
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val imageFileName = "JPEG_" + timeStamp + "_"
        val storageDir: File? = mCurrentActivity.getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        val image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",  /* suffix */
                storageDir /* directory */
        )

        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoPath = image.absolutePath
        return image
    }

    companion object {
        val instance = MyCamera()
    }
}