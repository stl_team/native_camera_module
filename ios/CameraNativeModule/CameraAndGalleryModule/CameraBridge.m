//
//  CameraBridge.m
//  CameraNativeModule
//
//  Created by admin on 06/01/2021.
//

#import <React/RCTBridgeModule.h>
#import <React/RCTViewManager.h>

@interface RCT_EXTERN_MODULE(CameraBridge, NSObject)
RCT_EXTERN_METHOD(ChangeViewBridge: (RCTPromiseResolveBlock)resolve rejecter:(RCTPromiseRejectBlock)reject)
@end
