//
//  CameraBridge.swift
//  CameraNativeModule
//
//  Created by admin on 06/01/2021.
//

import UIKit
import Foundation


@objc (CameraBridge)
class CameraBridge: NSObject {
  
  let appDelegate = UIApplication.shared.delegate as! AppDelegate
  var tempPro: RCTPromiseResolveBlock? = nil
  
  override init() {
    super.init()
    NotificationCenter.default.addObserver(self, selector: #selector(self.selectedImage), name: NSNotification.Name(rawValue: "Data"), object: nil)
  }
  
  //get and send image url to react native 
  @objc func selectedImage(notif: NSNotification) {
    
    if UserDefaults.standard .value(forKey: "selectedImage") as! String == "Gallery" {
      
      let UrlPath = notif.object as! URL
      let path:String = UrlPath.path
      print(UrlPath)
      print(path)
      
      tempPro!(path)
      
    } else {
      let UrlPath = notif.object as! String
      tempPro!(UrlPath)
    }
    
  }
  
  // Call method from React Native to capture camera
  @objc func ChangeViewBridge(_ resolve: @escaping RCTPromiseResolveBlock, rejecter reject: RCTPromiseRejectBlock) {
    
    tempPro = resolve
    appDelegate.goToRegisterView()
    
  }
  
}
