#import <React/RCTBridgeDelegate.h>
#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate, RCTBridgeDelegate>

@property (nonatomic, strong) UIWindow *window;
- (void) goToRegisterView;
-(NSString *)getCurrentTime;
- (UIImage *)scaleAndRotateImage:(UIImage *)image;
@end
