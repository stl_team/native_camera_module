#import "AppDelegate.h"
#import "CameraNativeModule-Swift.h"


#import <React/RCTBridge.h>
#import <React/RCTBundleURLProvider.h>
#import <React/RCTRootView.h>

#ifdef FB_SONARKIT_ENABLED
#import <SKIOSNetworkPlugin/SKIOSNetworkAdapter.h>
#endif
UIImagePickerController *imgPicker;
@implementation AppDelegate
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
#ifdef FB_SONARKIT_ENABLED
  
#endif
  
  RCTBridge *bridge = [[RCTBridge alloc] initWithDelegate:self launchOptions:launchOptions];
  RCTRootView *rootView = [[RCTRootView alloc] initWithBridge:bridge
                                                   moduleName:@"CameraNativeModule"
                                            initialProperties:nil];
  
  rootView.backgroundColor = [[UIColor alloc] initWithRed:1.0f green:1.0f blue:1.0f alpha:1];
  
  self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
  UIViewController *rootViewController = [UIViewController new];
  rootViewController.view = rootView;
  self.window.rootViewController = rootViewController;
  [self.window makeKeyAndVisible];
  
  imgPicker.delegate = self;
  
  return YES;
}


// this method will be called from the RCTBridge for openCamera
- (void) goToRegisterView {
  dispatch_async(dispatch_get_main_queue(), ^{
    
    [self openCamera];
    
  });
  
}

//this method for openCamera
-(void)openCamera
{
  if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
  {
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc]init];
    imagePicker.delegate = self;
    imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
    imagePicker.allowsEditing = YES;
    [self.window.rootViewController presentViewController:imagePicker animated:true completion:nil];
    
    
  }else{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Camera not available"
                                                   message:@"No Camera available on your device."
                                                  delegate:nil
                                         cancelButtonTitle:@"OK"
                                         otherButtonTitles:nil, nil];
    [alert show];
    alert = nil;
  }
}

//this method for openGallery
-(void)openGallery
{
  if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
  {
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc]init];
    imagePicker.delegate = self;
    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    imagePicker.allowsEditing = YES;
    [self.window.rootViewController presentViewController:imagePicker animated:true completion:nil];
    
  }else{
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Camera not available"
                                                   message:@"No Camera available on your device."
                                                  delegate:nil
                                         cancelButtonTitle:@"OK"
                                         otherButtonTitles:nil, nil];
    [alert show];
    alert = nil;
  }
}

//this is deligate method for finishPickingMediaInfo
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
  
  UIImage *pickedImage = [self scaleAndRotateImage:info[UIImagePickerControllerOriginalImage]];
  
  if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
  {
    
    //Save capture camera image in documentDirectories
    NSData *pngData = UIImagePNGRepresentation(pickedImage);
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0]; //Get the docs directory
    NSString *imageNewName = [NSString stringWithFormat:@"%@image.png",[self getCurrentTime]];
    NSString *filePath = [documentsPath stringByAppendingPathComponent:imageNewName]; //Add the file name
    [pngData writeToFile:filePath atomically:YES]; //Write the file
    
    NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
    //NSData *data = [NSKeyedArchiver archivedDataWithRootObject:path1];
    //[currentDefaults setObject:data forKey:@"selectedImage"];
    
    [currentDefaults setValue:@"Camera" forKey:@"selectedImage"];
    
    [[NSNotificationCenter defaultCenter]
     postNotificationName:@"Data"
     object:filePath];
    
  } else {
    
    if (@available(iOS 11.0, *)) {
      NSURL *path1 = [info valueForKey:UIImagePickerControllerImageURL];
      NSUserDefaults *currentDefaults = [NSUserDefaults standardUserDefaults];
      [currentDefaults setValue:@"Gallery" forKey:@"selectedImage"];
      
      [[NSNotificationCenter defaultCenter]
       postNotificationName:@"Data"
       object:path1];
      
    } else {
      // Fallback on earlier versions
      NSLog(@"Print Selected earlier versions Path");
    }
  }
  
  [self.window.rootViewController dismissViewControllerAnimated:YES completion:nil];
  
}

//get CurrentTime for rendome imageName
-(NSString *)getCurrentTime {
  NSDate *date = [NSDate date];
  NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
  [formatter setDateFormat:@"HHMMSS"];
  NSString *timeString = [formatter stringFromDate:date];
  return timeString;
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
  
  [self.window.rootViewController dismissViewControllerAnimated:YES completion:nil];
  
}

//this method for sacaleAndRotateImage
-(UIImage *)scaleAndRotateImage:(UIImage *)image
{
  int kMaxResolution = 320; // Or whatever
  
  CGImageRef imgRef = image.CGImage;
  
  CGFloat width = CGImageGetWidth(imgRef);
  CGFloat height = CGImageGetHeight(imgRef);
  
  CGAffineTransform transform = CGAffineTransformIdentity;
  CGRect bounds = CGRectMake(0, 0, width, height);
  if (width > kMaxResolution || height > kMaxResolution) {
    CGFloat ratio = width/height;
    if (ratio > 1) {
      bounds.size.width = kMaxResolution;
      bounds.size.height = bounds.size.width / ratio;
    }
    else {
      bounds.size.height = kMaxResolution;
      bounds.size.width = bounds.size.height * ratio;
    }
  }
  
  CGFloat scaleRatio = bounds.size.width / width;
  CGSize imageSize = CGSizeMake(CGImageGetWidth(imgRef), CGImageGetHeight(imgRef));
  CGFloat boundHeight;
  UIImageOrientation orient = image.imageOrientation;
  switch(orient) {
      
    case UIImageOrientationUp: //EXIF = 1
      transform = CGAffineTransformIdentity;
      break;
      
    case UIImageOrientationUpMirrored: //EXIF = 2
      transform = CGAffineTransformMakeTranslation(imageSize.width, 0.0);
      transform = CGAffineTransformScale(transform, -1.0, 1.0);
      break;
      
    case UIImageOrientationDown: //EXIF = 3
      transform = CGAffineTransformMakeTranslation(imageSize.width, imageSize.height);
      transform = CGAffineTransformRotate(transform, M_PI);
      break;
      
    case UIImageOrientationDownMirrored: //EXIF = 4
      transform = CGAffineTransformMakeTranslation(0.0, imageSize.height);
      transform = CGAffineTransformScale(transform, 1.0, -1.0);
      break;
      
    case UIImageOrientationLeftMirrored: //EXIF = 5
      boundHeight = bounds.size.height;
      bounds.size.height = bounds.size.width;
      bounds.size.width = boundHeight;
      transform = CGAffineTransformMakeTranslation(imageSize.height, imageSize.width);
      transform = CGAffineTransformScale(transform, -1.0, 1.0);
      transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
      break;
      
    case UIImageOrientationLeft: //EXIF = 6
      boundHeight = bounds.size.height;
      bounds.size.height = bounds.size.width;
      bounds.size.width = boundHeight;
      transform = CGAffineTransformMakeTranslation(0.0, imageSize.width);
      transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
      break;
      
    case UIImageOrientationRightMirrored: //EXIF = 7
      boundHeight = bounds.size.height;
      bounds.size.height = bounds.size.width;
      bounds.size.width = boundHeight;
      transform = CGAffineTransformMakeScale(-1.0, 1.0);
      transform = CGAffineTransformRotate(transform, M_PI / 2.0);
      break;
      
    case UIImageOrientationRight: //EXIF = 8
      boundHeight = bounds.size.height;
      bounds.size.height = bounds.size.width;
      bounds.size.width = boundHeight;
      transform = CGAffineTransformMakeTranslation(imageSize.height, 0.0);
      transform = CGAffineTransformRotate(transform, M_PI / 2.0);
      break;
      
    default:
      [NSException raise:NSInternalInconsistencyException format:@"Invalid image orientation"];
      
  }
  
  UIGraphicsBeginImageContext(bounds.size);
  
  CGContextRef context = UIGraphicsGetCurrentContext();
  
  if (orient == UIImageOrientationRight || orient == UIImageOrientationLeft) {
    CGContextScaleCTM(context, -scaleRatio, scaleRatio);
    CGContextTranslateCTM(context, -height, 0);
  }
  else {
    CGContextScaleCTM(context, scaleRatio, -scaleRatio);
    CGContextTranslateCTM(context, 0, -height);
  }
  
  CGContextConcatCTM(context, transform);
  
  CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, width, height), imgRef);
  UIImage *imageCopy = UIGraphicsGetImageFromCurrentImageContext();
  UIGraphicsEndImageContext();
  
  // [self setRotatedImage:imageCopy];
  return imageCopy;
}

- (NSURL *)sourceURLForBridge:(RCTBridge *)bridge
{
#if DEBUG
  return [[RCTBundleURLProvider sharedSettings] jsBundleURLForBundleRoot:@"index" fallbackResource:nil];
#else
  return [[NSBundle mainBundle] URLForResource:@"main" withExtension:@"jsbundle"];
#endif
}

@end
